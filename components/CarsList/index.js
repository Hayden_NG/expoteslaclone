import React from "react";
import { FlatList, Dimensions } from "react-native";
import { cars } from "./cars";
import { CarItem } from "../CarItem";

export const CarsList = () => {
  return (
    <FlatList
      data={cars}
      renderItem={({ item }) => <CarItem car={item} key={item.id} />}
      snapToAlignment={"start"}
      decelerationRate={"fast"}
      snapToInterval={Dimensions.get("window").height}
      showsVerticalScrollIndicator={false}
    />
  );
};
