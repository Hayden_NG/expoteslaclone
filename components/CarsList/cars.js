import { Images } from "../../images";

export const cars = [
  {
    id: 1,
    name: "Model S",
    tagline: "Starting at $69,420",
    image: Images.ModelS,
  },
  {
    id: 2,
    name: "Model 3",
    tagline: "Order Online for",
    taglineCTA: "Touchless Delivery",
    image: Images.Model3,
  },
  {
    id: 3,
    name: "Model X",
    tagline: "Order Online for",
    taglineCTA: "Touchless Delivery",
    image: Images.ModelX,
  },
  {
    id: 4,
    name: "Model Y",
    tagline: "Order Online for",
    taglineCTA: "Touchless Delivery",
    image: Images.ModelY,
  },
];
