import React from "react";
import { View, Pressable, Text } from "react-native";
import { styles } from "./styles";

export const StyledButton = (props) => {
  const { type, content, onPress } = props;

  const backgroundColo = type === "primary" ? "#171A20CC" : "#FFFFFFA6";
  const textColor = type === "primary" ? "#FFFFFF" : "#171A20CC";

  return (
    <View style={styles.container}>
      <Pressable
        style={[styles.button, { backgroundColor: backgroundColo }]}
        onPress={onPress}
      >
        <Text style={[styles.text, { color: textColor }]}>{content}</Text>
      </Pressable>
    </View>
  );
};
