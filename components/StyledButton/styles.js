import React from "react";
import { StyleSheet, Dimensions } from "react-native";

export const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    borderRadius: 20,
  },
  container: {
    width: Dimensions.get("window").width,
    padding: 16,
  },
  text: {
    fontSize: 12,
    fontWeight: "500",
    textTransform: "uppercase",
  },
});
