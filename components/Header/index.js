import React from "react";
import { View, Image } from "react-native";

import { styles } from "./styles";
import { Images } from "../../images";

export const Header = () => {
  return (
    <View style={styles.container}>
      <Image style={styles.logo} source={Images.Logo} />
      <Image style={styles.menu} source={Images.Menu} />
    </View>
  );
};
