import { Dimensions, StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    position: "absolute",
    justifyContent: "space-between",
    alignItems: "center",
    width: Dimensions.get("window").width,
    paddingHorizontal: 16,
    top: 50,
    zIndex: 100,
  },
  logo: {
    width: 100,
    height: 16,
    resizeMode: "contain",
  },
  menu: {
    width: 24,
    height: 24,
  },
});
