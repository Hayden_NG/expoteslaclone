export const Images = {
  ModelX: require("./assets/images/ModelX.jpeg"),
  Logo: require("./assets/images/logo.png"),
  Menu: require("./assets/images/menu.png"),
  Model3: require("./assets/images/Model3.jpeg"),
  ModelS: require("./assets/images/ModelS.jpeg"),
  ModelY: require("./assets/images/ModelY.jpeg"),
  SolarPanels: require("./assets/images/SolarPanels.jpeg"),
  SolarRoof: require("./assets/images/SolarRoof.jpeg"),
};
